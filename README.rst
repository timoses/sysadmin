.. Header levels:
   L1 L2 L3 L4 L5 L6
   == -- ~~ "" '' ``

.. contents:: **Contents**


Resources
=========

* `Awesome Sysadmin <https://github.com/kahun/awesome-sysadmin>`_



System Administration
=====================

Storage
-------
**SAN** (Storage): Storage Area Network (Fiber, iSCSI, Infiniband)
	**LUN**: Logical Unit Number
        Identifies a logical disk/volume which is a provisioned area of storage

* Storage
     + File storage
          - NAS (NFS, samba/cifs)
     + Block storage
     + Object storage

* HBA (Host Bus Adapter): piece of hardware which connects servers to storage network - decreases storage-specific workload on server

* Protocols
     + FC - Fiber Channel, typically used in SAN (Storage Area Network)
          - FCoE (Fiber Channel over Ethernet): Reduces cost as it allows using an existing IP network.
                * Requires network that supports FCoE (does not use TCP/IP)
                      * CNA - converged network adapter
                * newer: FCIP?
     + SCSI - Small Computer System Interface
          - SAS (serial attached scsi)?
          - iSCSI - Internet Small Computer System Interface
               * well suited for small businesses
     + Infiniband - very high bandwidth, very low latency -> super computers
     + AoE (ATA over Ethernet)

RAID (Redundant Array of Independent Disks)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* Features
  * Mirroring: Mirror data to multiple disks
  * Striping: Split data to multiple disks
  * Parity: Store redundancy check information
* Improves performance through parallel I/O to disks
* Levels
  * 0 - Only striping (+performance)
  * 1 - Only mirroring (+recovery)
  * 5 - rotating parity stripe [min: 3disks]
  * 6 - striping with dual parity [min: 4 disks]
  * 10 - mirror and stripe [min: 4disks]

Software-defined Storage
~~~~~~~~~~~~~~~~~~~~~~~~

Ceph
""""
Features:
* good for large amounts of files and high access rates

Architecture: docs.ceph.com/docs/jewel/architecture/

Components:
* `Monitor <http://docs.ceph.com/docs/jewel/rados/configuration/mon-config-ref/#>`_:
  * monitors keep cluster metadata
  * the more, the longer it takes to apply changes
  * odd number of monitors required (ensure quorum and prevent split-brain)
  * minimum 3 to allow for 1 node failure and still have 2 for consensus
  * `dataDirHostPath` for storing mon pod metadata on host -> restarting mon pod can quickly fetch it's previous data again
* RADOS – Ceph utilizes RADOS (Reliable Autonomic Distributed Object Store) to offer different storage types (block, file, object)
  * OSD (Object Storage Device) use existing storage on hosts/nodes to implement Ceph's RADOS object store. Each host/node or each storage device should be mapped to one OSD. OSDs implement data replication.
  * MON (Monitor) tracks the cluster's metadata (cluster map)
    * https://github.com/rook/rook/blob/master/design/mon-health.md
    * http://docs.ceph.com/docs/jewel/rados/configuration/mon-config-ref/#
* CRUSH (Controlled Replication Under Scalable Hashing) algorithm serves to compute storage locations (where can I find my data?)
  * Objects are assigned to PGs (placement groups). PGs are assigned to OSDs. This way, clients can refer to an object which stays in the same PG but may reside on different and changing OSDs. This adds a level of indirection from objects to actual storage location.
  * no centralized component required which would need to track where data can be found → vast scalability

Gluster
"""""""
Features:
* Geo Replication -> Disaster Recovery
* Easier setup
* large throughput with RAID for single files


Network
-------
**Switch**: Layer 2 (OSI):
    Build a table of MAC addresses (data link layer) and connected ports
        + direct traffic meant for one MAC address to only that port
        + MAC Address is found via ARP (Address Resolution Protocol)
                        (get data link layer address from internet layer address) (e.g. IP -> MAC)
        + Broadcast domain (forward broadcast messages to all ports)
        + => VLAN: Virtual LAN by means of adding a VLAN tag to data link layer which is resolved by each switch and only forwarded to those ports which belong to the VLAN (according to a switch setup/configuration)
            + -> build separate broadcast domains

**SDN** (Software-defined network)

[Structured cabling](https://en.wikipedia.org/wiki/Structured_cabling) - Standards for cabling buildings/campusses

Load balancing:
    L4:
        * terminating: terminates client TCP connection, buffers incoming bytes and writes them to a new connection to backend
        * passthrough: performs connection tracking and NAT to forward bytes from client. Requires no buffering as compared
          to 'terminating', also performs and scales better.
        * Architecture: "`fault tolerance and scaling via clustering and distributed consistent hashing <https://blog.envoyproxy.io/introduction-to-modern-network-load-balancing-and-proxying-a57f6ff80236#780e>`_"
            * `Consistent hashing <fault tolerance and scaling via clustering and distributed consistent hashing>`_ is used to to serve horizontally scalable edge routers and load balancers

    L7:
        ...

    Archticetures:
        * middle/edge proxy: sits between client and backends (or at the edge to internet)
            Direct Server Return (DSR): Egress traffic goes directly to client (bypasses LB)
                * connection tracking is limited a bit (no returned traffic to track state)
                * e.g. `GRE <https://en.wikipedia.org/wiki/Generic_Routing_Encapsulation>`_ for routing encapsulation
                  -> backend can decapsulate and see origninating client ip/port
        * client library: situated directly next to the client (downside: requires support for all programming languages used)
        * side car: situated next to client process -> Service Mesh

**VRF** (Virtual routing and forwarding)
    ? (https://en.wikipedia.org/wiki/Virtual_routing_and_forwarding)
    * In Cisco devices this is realized via `ASICs <https://en.wikipedia.org/wiki/Application-specific_integrated_circuit>`_ which can be programmed to serve as isolated routers

**VNF** (Virtual network function)
    ? (https://en.wikipedia.org/wiki/Network_function_virtualization)

**VTP** (VLAN Trunking Protocol)
    ? (https://en.wikipedia.org/wiki/VLAN_Trunking_Protocol)

**HSRP** (`Hot Standby Router Protocol <https://en.wikipedia.org/wiki/Hot_Standby_Router_Protocol>`_)
    * two devices for failover redundancy, one is on standby and takes over traffic if the other fails
    * Both use the same IP with a virtualized MAC address, only one is active and responds to ARP requests (?)
    * usually also both devices have their own specific IP address

Linux
~~~~~

* iptables
* ipvs
* (e)BPF
    * https://iovisor.github.io/bcc/
    *


IP
~~

Private IP Addresses: https://tools.ietf.org/html/rfc1918

VPN
~~~
VPNs are a concept of extending networks through Layer 2 or Layer 3 tunnels.

* IPSec Tunneling
* GRE (unencrypted)
* IP IP (IP in IP) (unencrypted)

NFV (Network function virtualization)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Usage of standard servers to implement network functionality which is usually performed by expensive dedicated hardware (e.g. Cisco ASA routers, expensive firewalls, ...). This can reduce cost requirements for performance are not as high.
VNFs (Virtualized Network Functions) are the software implementations.

Tools
~~~~~

* GNS3 (full-fledged Network simulator) - https://www.gns3.com/
* Packet Tracer - https://www.netacad.com/courses/packet-tracer

Observability
-------------
**Black-box monitoring**
    **Monitoring|Polling|Uptime** Testing externally visible behavior as a user would see it.
**White-box monitoring**
    **Metrics|Logs|Traces** Data from the internals of the system.
**Log rotation**
    In case of logging into files (vs to stderr/stdout) log file sizes should be monitored and eventually archived to prevent storage saturation. ([`logrotate`](https://manpages.debian.org/jessie/logrotate/logrotate.8.en.html))

Tools
~~~~~

Log Aggregation:
* fluentd
* splunk
* LogStash

Time Series Database:
* InfluxDB
* ElasticSearch

Visualization:
* Kibana
* Grafana

Monitoring:
* Icinga


Authentication
--------------
**IPA**
   FreeIPA - Centralized Identity Management System: Identity, Policy, Audit
**CAS**
   Central Authentication Service (https://www.apereo.org/projects/cas)
**LDAP**
   Lightweight Directory Access Protocol

Active Directory
~~~~~~~~~~~~~~~~

Tools:
* Apache Directory Studio

Management & Configuration
--------------------------

IAC
   Infrastructure as Code

IPMI
   (Management/Monitoring) Intelligent Platform Management Interface

   * Interface to Server/... hardware (rather than to an OS via ssh/...)
   * allows remote management of server, even installation of a new OS, or even if it is switched off
      * enables out-of-band management: use of dedicated channel for managing network devices
        (can be same as in-band or extra (lights-out management LOM) network connection to device)
SNMP
   (Management/Monitoring): Simple Network Management Protocol

   @Application layer ; used for network management and network monitoring
   "collecting and organizing information about managed devices on IP networks" [Wikipedia]

Config management tools
~~~~~~~~~~~~~~~~~~~~~~~
* ??? Packer, Salt(Stack) (Cloud)
* **Ansible** - Configure and manage inventory with playbooks (roles -> playbooks -> tasks -> modules)
        * client-only architecture
        * Ansible Galaxy: Hub for sharing roles
        * only client required (runs over ssh)
* **Puppet** - config, deployment, ...
    * client/server architecture *  Requires supporting infrastructure (master nodes, dbs)
    * Periodically checks if servers/inventory are still in desired state
    * r10k?
* **Terraform** - Infrastructure **orchestration**
    * client-only architecture
    * manages infrastructure on **cloud provider platforms**
    * not easily deployed on-premise
* **Chef**
    * client/server architecture
* ? Packer.io, Saltstack, Confd
* https://www.upguard.com/articles/the-7-configuration-management-tools-you-need-to-know

Package management
~~~~~~~~~~~~~~~~~~

RPM
"""

Local Repository
''''''''''''''''
* Pulp
* mrepo


DNS
---
https://kb.pressable.com/article/dns-record-types-explained/


PKI
---
* ACME (Automatic Certificate Management Environment): Protocol to automatice certificate issuance, and other functions
    * used by `CertBot <https://certbot.eff.org/>`_

* `Baseline Requirements <https://cabforum.org/baseline-requirements-documents/>`_ defines PKI best practices (?)


Bootstrapping
-------------

PXE
~~~

* Historically: PXE with TFTP
* Newer: iPXE with (HTTP,Fiber,...)

Bootloader
~~~~~~~~~~
* Grub(2)
* Das U-Boot (often on embedded systems)
* lilo (obsolete Linux bootloader)


Logging
-------

Syslog
~~~~~~

* `RFC 5424 <https://tools.ietf.org/html/rfc5424>`_


Security
--------

Audit
~~~~~

Linux audit system.

Rule types:
* Control - configure behaviour of Audit
* File system - file/directory access auditing
* System call - auditing system calls

Tools
"""""
* auditctl
* ausearch
* aureport

Linux Security Modules (LSM)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* SELinux
* AppArmor
* Smack
* Tomoyo

SELinux
"""""""
* Contexts: User, Role, Type, Level
  * Labels applied to
    * files, directories (by extended attributes)
    * ports, processes
* Rule
* Policy: Set of rules
  * only one can be active at any one time
  *  e.g.: targeted, minimum, MLS


Software Architectures
======================

SOA
   Service-oriented architecture.
   Implementation examples: REST, SOAP, CORBA


Virtualization
==============

* Emulation - Virtualization completely in software (can translate between various CPU architectures)
  * e.g. Qemu
    * Qemu can be integrated with hypervisors such as KVM for improved virtualization performance
* CPU Virtualization extensions allow **full virtualization**
  * e.g. Intel: (I)VT | AMD-V

Commands
--------

Create new VM with ISO:
```
virt-install --name=debian --vcpus=1 --memory=1024 --cdrom=/tmp/debian-9.0.0-amd64-netinst.iso --disk size=5 --os-variant=debian8
```

Manage VM with `virsh`.

Connect to VM with `virt-viewer`.


Linux
=====

* FHS - Filesystem Hierarchy Standard

Shell
-----
* Redirection

File times
----------
* Access: Content was accessed
* Modify: Content was modified
* Change: Metadata was updated

I/O Scheduler
-------------
* Provides interface between generic block layer and low-level physical device drivers.
* Example: VM (Virtual Memory) and VFS (Virtual File System) layers submit I/O requests to block devices
* Important facts:
  * Minimize hardware access time (reduce strain on storage devices), sort writes by write locations
  * Merge write access requests (writes should be as big and contiguous as possible)
  * Low latency for requests
    * especially for read requests
  * fair sharing of I/O bandwidth between processes
* Files:
  * /sys/block/<dev>/queue/scheduler - Scheduler used (change with 'echo <sched> > ...)
  * /sys/block/<dev>/queue/iosched - Tunables

Init
----
* old: SysVInit (System V)
  * runlevel
* SystemD

DevOps
======

* [Twelve-Factor App](https://12factor.net/)

Security
========

Virus scanner
-------------

* rkhunter
* chrootkit
































CheatSheet
==========

Software packages
-----------------
* Monitoring:
    * procps
    * sysstat

Network
-------
* `Network inspection <https://www.dynatrace.com/news/blog/detecting-network-errors-impact-on-services/>`_
     * ifconfig - display network interfaces info (IP, mask) and RX/TX statistics
          * more enhanced: ip
     * ethtool - display ethernet info (link speed, ...)
     * netstat -s - display summary statistics
          * more modern: ss
     * nmcli - control NetworkManager
           * nmtui
     * tcpdump / wireshark
     * ncat - netcat

* iptraf
* mtr
* traceroute
* route
* ip
* ping

* nmap

* dhclient - DHCP

HTTP
~~~~

* :code:`no_proxy=cafe.example.com curl --resolve cafe.example.com:443:$INGRESS_IP https://cafe.example.com:443/coffee --insecure`
* :code:`no_proxy=cafe.example.com curl -k -HHost:cafe.example.com --connect-to cafe.example.com::$INGRESS_IP:443 https://cafe.example.com/coffee`
* :code:`openssl s_client -servername cafe.example.com -connect $INGRESS_IP:443`

DNS
~~~
* host
* nslookup
* dig
* DNS Book: http://www.zytrax.com/books/dns/

Ping
~~~~

Monitor
~~~~~~~
* :code:`tcpdump ip proto \\icmp`
* :code:`iptables -I INPUT -p icmp --icmp-type 8 -m state  --state NEW,ESTABLISHED,RELATED -j LOG --log-level=1 --log-prefix "Ping Request "`

Firewall
~~~~~~~~

firewall-cmd
""""""""""""

* :code:`--list--all-zones`: List all zones and its properties
* Types:
    * :code:`zone`
    * :code:`service`
    * :code:`icmptype`
    * :code:`helper`
* Type operations:
    * :code:`--get-<type>s`: List available pre-defined type objects
    * :code:`--info-<type> <obj_name>`: List detailled information about object
* :code:`--direct`: Allows direct iptables commands

Storage
-------

* hdparm --fibmap <file>
      * Display occupying sectors for a file

* filefrag

* debugfs

* df
* du

Partitioning
~~~~~~~~~~~~
* fdisk
* gdisk (for gpt)
* sfdisk (script tool)
* lsblk
* blkid
* parted

* mount
    * automount | autofs - automatically mount when mount point accessed
    * systemd: x-systemd.automount | x-systemd.device-timeout | ... -> systemctl daemon-reload && systemctl restart local-fs.target
* findmnt

* cryptsetup | /etc/crypttab (Info on decryption of devices at boot)

LVM
~~~
* `lvmdiskscan`
* Physical Volumes: `pvs`
* Volume Groups: `vgs`
* Logical Volumes: `lvs`

RAID
~~~~
* mdadm
* mdmon

Filesystems
~~~~~~~~~~~
* mkfs[.*]
* fsck[.*]
  * touch /forcefsck && reboot
* findfs
* e2label
* dumpe2fs | tune2fs

* mkswap
* swapon/off

* quota (edquota, quotaon/off, quotacheck, repquota, ...)

* ln
  * realpath / readlink

Backup
~~~~~~
* dump
* bacula
* amanda


Scheduling
----------
* at | atq (queue)
* crontab
* sleep

Jobs & Processes
----------------

Process **context**: State of CPU registers, process's memory state, location of execution in program, ...

Scheduling / context switching: Kernel will switches contexts to switch between processes/threads

* pgrep
* ps | pstree
* (h)top
* renice - set priority (niceness)
* strace - trace system calls made by process
* pmap - Process Memory Map

* ulimit - (shell built-in) display/set shell process resource limits (only effective in current shell)
    * edit `/etc/security/limits.conf` for persistent changes
* nohup
* bg fg jobs (CTRL-Z)

IPC (Inter Process Communication)
---------------------------------
* ipcs

Libraries
~~~~~~~~~
* ldd

DBUS
----
* busctl
* gdbus

CLI Tools
---------
* whatis (man  -f)
* apropos (man -k)
* man -wK

* whereis - locates through binary, source and man pages
* which - display full path of binary
* locate (updatedb to update the db)

* locale

* xargs: build commands from stdin

Files
~~~~~
* stat - display file's inode information
* lsof | fuser

Attributes
""""""""""
* lsattr
* chattr

ACL
"""
(denoted by '+' in ls -l)
* getfacl
* setfacl

Extended Attributes
"""""""""""""""""""
(
(man xattr | attr)
* attr
* getfattr
* setfattr


Copy (apropos)
""""""""""""""
* cp
* dd
* rsync

Compress (apropos)
""""""""""""""""""
* gzip (very fast)
* bzip2 (very good compression, takes little longer)
* xz (even better compression, takes longer)

* tar
* cpio

Search
""""""
* grep
* strings - search for readable text in binary file

Compare (apropos)
"""""""""""""""""
* diff | diff3 (text)
* cmp (binary)
* comm

Manipulation
""""""""""""
* cat | tac
* rev
* tee
* cut
* tr
* sed | awk
* sort
* uniq
* paste
* join
* split
* fold
* patch
* expand

* printf
* fmt
* groff

Users & Groups
~~~~~~~~~~~~~~
* w
* who
* last
* passwd
* chage
* [l]id

Documentation
~~~~~~~~~~~~~
* man
* whatis (man -f)
* apropos (man -k)
* info (GNU info)


System performance
~~~~~~~~~~~~~~~~~~
* [h]top
* free | vmstat
* nproc
* uptime

* iostat | iotop | ionice
* mpstat
* numastat - Non-Uniform Memory Architecture
* sar - System Activity Report

* stress - Put stress on system

Kernel
""""""
* [perf](http://man7.org/linux/man-pages/man1/perf.1.html)
* dmesg - display messages (kernel) (/var/log/messages)
* Parameters:
  * man bootparam
  * man kernel-command-line


Package management
~~~~~~~~~~~~~~~~~~

* RPM
     * rpm2cpio
     * repoquery (dnf repoquery)

Virtualization
~~~~~~~~~~~~~~

* libvirt tools:
  * virsh
  * virt-manager (graphical)
  * virt-viewer
  * virt-install
* virt-what
* qemu-*

x509
----
* Tools:
    * cfssl
    * easy-rsa
    * openssl
        * view: :code:`openssl x509 -in <certificate> -text -noout [-inform der (if der binary format)]`
        * transform: :code:`openssl x509 -in <certin> [-inform der] -outform <pem/der> -out <certout>`

Shell
-----

Heredoc
~~~~~~~
.. code::

   cat << limitstring
   line1
   limitstring

Suppress leading tabs (not spaces) with :code:`-`

.. code::

   cat << -limitstring

Suppress parameter substitution with :code:`"`

.. code::

   cat << "limitstring"
   $I_AM_NOT_SUBSTITUTED


Logging
-------

Logfiles
~~~~~~~~
* /var/log/messages
* /var/log/secure

Syslog
~~~~~~
* rsyslogd:
    * Print Priority (Facility/Severity):

      .. code::

          $template TraditionalFormatWithPRI,"%pri-text%: %timegenerated% %HOSTNAME% %syslogtag%%msg:::drop-last-lf%\n"
          *.* /var/log/testpri;TraditionalFormatWithPRI

Devices & Hardware
------------------
* lsusb
* lshw

Configuration
-------------
* sysctl - get/set parameters under /proc/sys (persistent in /etc/sysctl.{d/,conf})
* modprobe - enable/disable modules (config in /etc/modprobe.d)
  * /lib/modules/
  * modinfo
  * lsmod
* grub ... /proc/cmdline (kernel parameters booted with)

Security
--------

SELinux
~~~~~~~
* secon
* chcon
* ls -Z
* ps -Z
* mv/cp -Z (set context to default of destination directory)
  * restorecon
* semanage (from policycoreutils-python-utils pkg)
* Policy Parameters
  * setsebool
  * getsebool
* setroubleshoot-server

Rescue
------
* livecd
* boot with kernel param "emergency" or "single"
